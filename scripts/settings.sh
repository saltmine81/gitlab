#!/usr/bin/env bash

# Sourcing script
#
# https://gitlab.com/saltmine81

### Functions
# dmenu theme
dmenu_theme() {
    dmenu_colors="-h 12 -nf #928374 -nb #282828 -sf #ebdbb2 -sb #458588"
}

### Colors
# Gruvbox color scheme
# https://github.com/morhetz/gruvbox
# black           = "#282828"
# white           = "#ebdbb2"
# red             = "#cc241d"
# green           = "#98971a"
# yellow          = "#d79921"
# blue            = "#458588"
# purple          = "#b16286"
# aqua            = "#689d6a"
# gray            = "#928374"
# orange          = "#d65d0e"
