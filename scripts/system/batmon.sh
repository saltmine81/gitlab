#!/usr/bin/env bash

# Battery monitoring script
#
# https://gitlab.com/saltmine81

# Variables
# Battery ID, see /sys/class/power_supply/
bat_id="BAT0"
low_level=10
high_level=85

# Function
main() {
while true; do

# Current battery level and status
bat_level=$(/usr/bin/cat /sys/class/power_supply/$bat_id/capacity)
bat_status=$(/usr/bin/cat /sys/class/power_supply/$bat_id/status)

# If unplugged and low capacity
    if
        [ $bat_level -le $low_level -a $bat_status = "Discharging" ]
    then
        /usr/bin/notify-send -u critical -t 3000 "Battery" "Battery level is $bat_level% and discharging"
    fi

# If plugged and high capacity
    if
        [ $bat_level -ge $high_level -a $bat_status = "Charging" ]
    then
        /usr/bin/notify-send -t 3000 "Battery" "Battery level is $bat_level% and charging"
    fi

sleep 150

done
}

main
