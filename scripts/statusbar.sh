#!/usr/bin/env bash

# A simple statusbar script

mem() {
    memory=$(free --mega | awk '/Mem:/ {print $3}')
    printf "Mem: %sM" $memory
}

vol() {
    volume=$(pamixer --get-volume-human)
    printf "Vol: %s" $volume
}

bat() {
    battery=$(cat /sys/class/power_supply/BAT0/capacity)
    printf "Bat: %s%%" $battery
}

cur_date() {
    printf "%(%A, %e %b)T"
}

cur_time() {
    printf "%(%H:%M)T"
}

main() {
	while true; do
        xsetroot -name " $(mem) | $(vol) | $(bat) | $(cur_date) | $(cur_time) "
		sleep 5
	done
}

main
