#!/usr/bin/env bash

# Dmenu script for mountin/unmounting external drives
#
# https://gitlab.com/saltmine81

### Source
. $HOME/gitlab/scripts/settings.sh

### Functions
mount_unmount() {
    cmd=$(printf "mount\nunmount\nlist\n" \
        | dmenu $dmenu_colors -p "Mount menu:" $*)

    if [ -z "$cmd" ]; then
	    exit 0
    fi

    case "$cmd" in
	    mount)
            notify-send "Mount" \
                "$(udisksctl mount -b $(lsblk -lpo NAME,SIZE,FSTYPE,LABEL,TYPE \
                | awk '/part/ {print $1, "(" $4, "/ " $3, "/ " $2 ")"}' \
                | awk '$0 !~ /nvme0n1/' \
                | dmenu $dmenu_colors -l 5 -p "Device to mount:"))" ;;
	    unmount)
		    notify-send "Unmount" \
                "$(udisksctl unmount -b $(lsblk -lpo NAME,SIZE,FSTYPE,LABEL,TYPE \
                | awk '/part/ {print $1, "(" $4, "/ " $3, "/" $2 ")"}' \
                | awk '$0 !~ /nvme0n1/' \
                | dmenu $dmenu_colors -l 5 -p "Device to unmount:"))" ;;
        list)
            notify-send "Device list" \
                "$(lsblk -lpo NAME,LABEL,SIZE,FSTYPE,MOUNTPOINT)" ;;
	    *)
		    printf "Option not recognized: %s\n" "$cmd" >&2
    esac
    #udisksctl power-off -b $(lsblk -lp | awk '/part/ {print $1, "(" $4 ")"}' | dmenu -p poweroff)
}

main() {
    dmenu_theme
    mount_unmount
}

main
