#!/usr/bin/env bash

# Script for reading bookmarks file, selecting in dmenu and open in a browser
# Add bookmark by typing a link not present in bookmarks file
#
# https://gitlab.com/saltmine81

# Command for including history
# sqlite3 .local/share/qutebrowser/history.sqlite "SELECT * FROM History;" | cut -d "|" -f 1

### Source
. $HOME/gitlab/scripts/settings.sh

### Variables
bookmarks="$HOME/crypt/urls"

### Functions

show_bookmarks() {
    url=`sort $bookmarks \
        | dmenu -l 10 $dmenu_colors -p "Select bookmark:"`
}

open_bookmark() {
    # firefox -new-tab $url
    qutebrowser --target tab $url
}

add_bookmark() {
    if [ "$(grep $url $bookmarks)" != "$url" ] ; then
        echo $url >> $bookmarks
    fi
}

main(){
    dmenu_theme
    show_bookmarks
    open_bookmark
    add_bookmark
}

main
