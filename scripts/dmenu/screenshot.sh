#!/usr/bin/env bash

# Screenshot script for dmenu using scrot
#
# https://gitlab.com/saltmine81

### Source
. $HOME/gitlab/scripts/settings.sh

### Variables
screenshot_folder="$HOME/pictures/screenshots"
screenshot_filename="$screenshot_folder/scrot-%Y%m%d-%H%M%S.png"


### Functions
create_screenshot_folder() {
    if [ ! -d "$screenshot_folder" ]; then
        mkdir -p $screenshot_folder
    fi
}

make_screenshot() {
    cmd=$(printf "selection\nfullscreen\nfullscreen 3s delay\n" \
        | dmenu -h 12 $dmenu_colors -p "Screenshot:" $*)

    if [ -z "$cmd" ]; then
	    exit 0
    fi

    case "$cmd" in
	    selection)
		    scrot -s $screenshot_filename ;;
	    fullscreen)
		    scrot $screenshot_filename ;;
	    "fullscreen 3s delay")
		    scrot -d 3 $screenshot_filename ;;
	    *)
		    printf "Option not recognized: %s\n" "$cmd" >&2
    esac
}

main() {
    create_screenshot_folder
    dmenu_theme
    make_screenshot
}

main
