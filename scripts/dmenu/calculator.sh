#!/usr/bin/env bash

# A simple dmenu calculator
#
# https://gitlab.com/saltmine81

### Source
. $HOME/gitlab/scripts/settings.sh

### Functions
calculate() {
    $1 \
        | dmenu $dmenu_colors -p "Calculator:" \
        | bc -l \
        | xclip
}

# Main application
main(){
    dmenu_theme
    calculate
}

main
