#!/usr/bin/env bash

# Dmenu script for monitor settings
#
# https://gitlab.com/saltmine81

### Source
. $HOME/gitlab/scripts/settings.sh

### Functions
set_monitor() {
    cmd=$(printf "laptop only\ndual monitors\n" | dmenu $dmenu_colors -p "Monitor setup:" $*)

    if [ -z "$cmd" ]; then
	    exit 0
    fi

    case "$cmd" in
	    "laptop only")
		    xrandr --output eDP --primary --mode 1920x1080 --output HDMI-1 --off & ;;
	    "dual monitors")
		    xrandr --output DP-1 --primary --mode 1920x1080 --output HDMI-1 --auto --left-of eDP & ;;
        *)
		    printf "Option not recognized: %s\n" "$cmd" >&2
    esac
}

main() {
    dmenu_theme
    set_monitor
}

main
