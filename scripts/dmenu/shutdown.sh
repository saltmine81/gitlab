#!/usr/bin/env bash

# Shutdown script for dmenu
#
# https://gitlab.com/saltmine81

### Source
. $HOME/gitlab/scripts/settings.sh

### Functions
shutdown_menu() {
    cmd=$(printf "suspend\npoweroff\nexit dwm\nreboot\n" \
        | dmenu -h 12 $dmenu_colors -p "Shutdown menu:" $*)

    if [ -z "$cmd" ]; then
	    exit 0
    fi

    case "$cmd" in
	    poweroff)
		    systemctl poweroff ;;
	    reboot)
		    systemctl reboot ;;
	    suspend)
		    systemctl suspend ;;
	    "exit dwm")
		    pkill -15 Xorg ;;
	    *)
		    printf "Option not recognized: %s\n" "$cmd" >&2
    esac
}

main() {
    dmenu_theme
    shutdown_menu
}

main
