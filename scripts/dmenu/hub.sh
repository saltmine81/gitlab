#!/usr/bin/env bash

# A dmenu hub script
# 
# https://gitlab.com/saltmine81

### Source
. $HOME/gitlab/scripts/settings.sh

### Variables
script_path="$HOME/gitlab/scripts/dmenu"


### Functions
dmenu_execute() {
    run_dmenu="$(find $script_path/* \
        -type f ! -name 'hub.sh' -exec basename {} .sh \; \
            | sort \
            | dmenu $dmenu_colors -p "Hub:")"

    eval "$script_path/$run_dmenu.sh"

    exit 0
}

main(){
    dmenu_theme
    dmenu_execute
}

main
