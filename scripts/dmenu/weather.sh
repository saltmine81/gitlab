#!/usr/bin/env bash

# Get the current weather
#
# https://gitlab.com/saltmine81

### Source
. $HOME/gitlab/scripts/settings.sh

### Variables
location="Ljungby"

### Functions

get_weather() {
    cmd=$(printf "$location\n" | dmenu $dmenu_colors -p "Weather:" $*)

    if [ -z "$cmd" ]; then
	    exit 0
    fi

    case "$cmd" in
	    Ljungby)
            notify-send "Weather in $location" "$(curl -s wttr.in/$location?format="+%C,+%t\n" | sed -e 's/^[ ]*//')" ;;
	    *)
		    printf "Option not recognized: %s\n" "$cmd" >&2
    esac
}

main() {
    dmenu_theme
    get_weather
}

main
