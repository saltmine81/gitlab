#!/usr/bin/env bash

# Script for getting username and/or password.
# Also edit the password file, or secure note file
#
# https://gitlab.com/saltmine81

### Source
# For calling $dmenu_colors in function dmenu_theme
. $HOME/gitlab/scripts/settings.sh

### Variables
password_file="$HOME/crypt/passwords"
notes_file="$HOME/crypt/notes"

### Functions / Commands
init() {
    user_pass=$(printf "password\nusername\nnotes\nedit password" \
        | dmenu $dmenu_colors -p "Passnote:" $*)
}

select_item() {
    item=$(cut -d " " -f 3 $password_file \
        | dmenu $dmenu_colors -p "Select item:")
}

get_or_edit() {
    case "$user_pass" in
        password)
            select_item
            pass=$(grep -w $item $password_file \
                | cut -d " " -f 2)
            printf "%s" $pass | xclip -selection clipboard
            notify-send "Passnote" "Password for $item copied to clipboard" ;;
        username)
            select_item
            user=$(grep -w $item $password_file \
                | cut -d " " -f 1)
            printf "%s" $user | xclip -selection clipboard
            notify-send "Passnote" "Username for $item copied to clipboard" ;;
        notes)
            st sh -c "nvim $notes_file" ;;
        "edit password")
            st sh -c "nvim $password_file" ;;
        *)
		    printf "Option not recognized: %s\n" "$cmd" >&2
    esac
}

main(){
    dmenu_theme
    init
    get_or_edit
}

main
