# .zshrc
#
# Magnus Ivarsson 

clear
$HOME/gitlab/scripts/terminal-color/bars
echo " $(zsh --version | awk 'NR==1{print $1,$2}')\n"

# Path
export PATH=$HOME/bin:/usr/bin:/usr/local/bin:$PATH

# Editor
export VISUAL=nvim;
export EDITOR=nvim;

# Manpager
export MANPAGER="nvim -c 'set ft=man' -"

# Set window title (user@domain: pwd)
case $TERM in
    xterm*)
        precmd () {print -Pn "\e]0;%n@%m: %~\a"}
        ;;
esac

# History
HISTFILE=$HOME/.cache/zsh_history
HISTSIZE=1000
SAVEHIST=1000
setopt hist_ignore_dups
setopt hist_ignore_space
setopt inc_append_history
setopt share_history

# Alias
alias ls='ls --color=auto'                  # ls with color
alias vim='nvim'                            # start nvim instead
alias cp='cp -iv'                           # interactive
alias grep='grep -i --color=auto'           # ignore case, color output
alias mv='mv -iv'                           # interactive
alias feh='feh --image-bg #282828 -Z -.'    # feh alias for nicer background and zoomed to fit
alias rm='rm -v'                            # verbose

# Prompts
# 0 black
# 1 red
# 2 green
# 3 yellow
# 4 blue
# 5 magenta
# 6 cyan
# 7 white
PROMPT='%F{cyan}%~%f %B%F{yellow}>>>%f%b '

# Completion
autoload -Uz compinit
compinit -d $HOME/.cache/zcompdump

zstyle :compinstall filename '/home/magnus/.zshrc'

# ls colors
LS_COLORS="$LS_COLORS:di=1;33:ln=1;36"
export LS_COLORS

# extract - archive extractor
# usage: extract <file>
extract ()
{
  if [ -f $1 ] ; then
    case $1 in
      *.tar.bz2)   tar xjf $1   ;;
      *.tar.gz)    tar xzf $1   ;;
      *.bz2)       bunzip2 $1   ;;
      *.rar)       unrar x $1     ;;
      *.gz)        gunzip $1    ;;
      *.tar)       tar xf $1    ;;
      *.tbz2)      tar xjf $1   ;;
      *.tgz)       tar xzf $1   ;;
      *.zip)       unzip $1     ;;
      *.Z)         uncompress $1;;
      *.7z)        7z x $1      ;;
      *)           echo "'$1' cannot be extracted via extract" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}

# compress
compress() {
    local DATE="$(date +%Y%m%d-%H%M%S)"
    tar cvzf "$DATE.tar.gz" "$@"
}

# Misc
setopt nobeep
setopt nonomatch                # * as wildcard in for example nmap

bindkey -v

# Autostart dwm
if [ -z "${DISPLAY}" ] && [ "${XDG_VTNR}" -eq 1 ]; then
  exec startx
fi
