#
# ~/.bash_profile
#

# Auto start sway when loggin in
if [ -z $DISPLAY ] && [ "$(tty)" = "/dev/tty1" ]; then
  exec sway
fi

# Auto start dwm when logging in
#[[ -f ~/.bashrc ]] && . ~/.bashrc
#
#if [ -z "${DISPLAY}" ] && [ "${XDG_VTNR}" -eq 1 ]; then
#  exec startx
#fi
