#
# ~/.bashrc
#

# Path
export PATH="$PATH:/usr/local/bin:/bin:/usr/bin:$HOME/gitlab/scripts/system:$HOME/gitlab/scripts/tools"

# Manpager
export MANPAGER='nvim +Man!'
export MANWIDTH=999

# fzf
export FZF_DEFAULT_OPTS="--height=40% --info=inline --layout=reverse --pointer='>' --no-separator \
    --color=fg:white,fg+:white:regular,hl:cyan,hl+:cyan:regular,prompt:cyan,pointer:cyan,info:cyan"

# fff
export FFF_HIDDEN=1         # Show hidden files
export FFF_COL2=6           # Status line bg color
export FFF_COL3=4           # Selected color
export FFF_COL4=6           # Cursor color
export FFF_COL5=0           # Status line fg color

# Other exports
export VISUAL=nvim
export EDITOR=nvim
export GREP_COLORS='ms=1;36'

# Source
source /usr/share/doc/pkgfile/command-not-found.bash

# Use bash-completion, if available
[[ $PS1 && -f /usr/share/bash-completion/bash_completion ]] && \
    . /usr/share/bash-completion/bash_completion

# Binds
bind "set show-all-if-ambiguous on"
# If there are multiple matches for completion, Tab should cycle through them
bind TAB:menu-complete
bind "set mark-symlinked-directories on"

set -o noclobber

# Misc
complete -cf doas	# Enable autocomplete when doas

# Welcome
clear
printf "\e[1;36m%s@%s\e[0m\n" $USER $HOSTNAME 
printf "\e[0;36mArch, %s\e[0m\n" $(uname -r)
printf "\e[0;36m$(bash --version \
    | awk 'NR==1{print $1,$2,$3,$4}' \
    | cut -d "(" -f 1)\e[0m\n\n"

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# History
export HISTFILE=$HOME/.cache/.bash_history
export HISTCONTROL=ignoreboth:erasedups
shopt -s histappend
export HISTSIZE=10000
export HISTFILESIZE=10000
export PROMPT_COMMAND='history -a'

# Aliases
alias ls='ls --color=auto'
alias cp='cp -iv'
alias grep='grep -i --color=auto'
alias mv='mv -iv'
alias rm='rm -iv'
alias lynx='lynx --vikeys'

# Prompt
# \[...\] to avoid problems when arrow up/down
PS1="\[\e[1;34m\]\h\[\e[0m\] \[\e[0;36m\]\w\[\e[0m\]\[\e[1;34m\]\n> \[\e[0m\]"

# Functions

# extract - archive extractor
# usage: extract <file>
extract ()
{
  if [ -f $1 ] ; then
    case $1 in
      *.tar.xz)    tar xf $1    ;;
      *.tar.zst)   unzstd $1    ;;
      *.tar.bz2)   tar xjf $1   ;;
      *.tar.gz)    tar xzf $1   ;;
      *.bz2)       bunzip2 $1   ;;
      *.rar)       unrar x $1   ;;
      *.gz)        gunzip $1    ;;
      *.tar)       tar xf $1    ;;
      *.tbz2)      tar xjf $1   ;;
      *.tgz)       tar xzf $1   ;;
      *.zip)       unzip $1     ;;
      *.Z)         uncompress $1;;
      *.7z)        7z x $1      ;;
      *)           printf "%s cannot be extracted via extract\n" $1 ;;
    esac
  else
    printf "%s is not a valid file\n" $1
  fi
}

# Go to directory, using fzf
go() {
    cd $HOME && cd "$(find -type d | fzf)"
}

# Search pacman packages
pacsearch() {
    pacman -Ssq \
        | fzf -m --preview="pacman -Si {}" \
        --preview-window=:hidden --bind=space:toggle-preview
}

# Search command history
hist() {
    hist_item=$(history \
        | fzf --prompt="History items: ")
    hist_item_trim=$(echo $hist_item | cut -d " " -f 2-)
    wl-copy "$hist_item_trim"
}

# Encrypt or decrypt files, using gpg
crypt() {
    if [ -f $2 ]; then
        case $1 in
            -e) gpg -c --pinentry-mode loopback $2;;
            -d) gpg --pinentry-mode loopback $2;;
            *) printf "Unknown option\n";;
        esac
    fi
}

# Show git status for all my repos
gsa() {
    current_dir=$(pwd)
    
    printf ":: Git status for all repos:\n"

    for item in $(find $HOME -type d -name ".git");
    do
        path=$(dirname $item)
        if [[ -n $(grep saltmine81 $path/.git/config) ]]; then
            printf "\e[0;33m>  %s\e[0m\n" $(basename $path)
            cd $path 
            git status -s
            printf "\n"
        fi
    done
    
    cd $current_dir
}

# ls colors
LS_COLORS="$LS_COLORS:di=1;36:ln=4;34:ex=0;33"
export LS_COLORS
