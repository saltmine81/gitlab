" Neovim
"
" init.vim
"
" Magnus Ivarsson
" Location: ~/.config/nvim/

" General
set nocompatible
syntax on
"set cursorline
set mouse=a
set mousehide
set wrap
set wildmenu
"set number
set ruler
set clipboard=unnamedplus
set nobackup
set noswapfile
set scrolloff=10

" Tab indent
set expandtab
set smarttab
set shiftwidth=4
set tabstop=4

" Remap split navigation
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l

" Do not use arrowkeys
nnoremap <Left>	:echoe "Use h"<CR>
nnoremap <Right> :echoe "Use l"<CR>
nnoremap <Up> :echoe "Use k"<CR>
nnoremap <Down> :echoe "Use j"<CR>

" Search
set ignorecase
set smartcase
set hlsearch
set incsearch
" Turn off search highlighting with <CR> (carriage-return)
nnoremap <CR> :nohlsearch<CR><CR>

" <Leader> : normally \  change with :let mapleader = ","
let mapleader = ","

" Colors
" Based on the Archery color scheme
" https://github.com/Badacadabra/vim-archery

" Fix for breaking colors in 0.10.0
colorscheme vim
set notermguicolors

" Comment
hi Comment cterm=italic ctermfg=7 ctermbg=none

" Constant
hi Constant cterm=none ctermfg=14 ctermbg=none

hi String cterm=none ctermfg=15 ctermbg=none

hi link Character Constant
hi link Number    Constant
hi link Boolean   Constant
hi link Float     Constant

" Identifier
hi Identifier cterm=none ctermfg=6 ctermbg=none

hi link Function Identifier

" Statement
hi Statement cterm=bold ctermfg=4 ctermbg=none

hi link Conditional Statement
hi link Repeat      Statement
hi link Label       Statement
hi link Operator    Statement
hi link Keyword     Statement
hi link Exception   Statement

" PreProc
hi PreProc cterm=none ctermfg=15 ctermbg=none

hi link Include   PreProc
hi link Define    PreProc
hi link Macro     PreProc
hi link PreCondit PreProc

" Type
hi Type cterm=none ctermfg=4 ctermbg=none

hi link StorageClass Type
hi link Structure    Type
hi link Typedef      Type

" Special
hi Special cterm=none ctermfg=7 ctermbg=none

hi link SpecialChar    Special
hi link Tag            Special
hi link Delimiter      Special
hi link SpecialComment Special
hi link Debug          Special

" Underlined
hi Underlined cterm=underline ctermfg=none ctermbg=none

" Ignore
hi Ignore cterm=none ctermfg=none ctermbg=none

" Error
hi Error cterm=none ctermfg=9 ctermbg=0

" Todo
hi Todo cterm=none ctermfg=11 ctermbg=0

" Modes
hi Normal       cterm=none ctermfg=12   ctermbg=none
hi Visual       cterm=none ctermfg=0    ctermbg=12
hi VisualNOS    cterm=none ctermfg=15   ctermbg=4

" Cursor
hi Cursor       cterm=none ctermfg=15   ctermbg=4
hi CursorIM     cterm=none ctermfg=15   ctermbg=4
hi CursorColumn cterm=none ctermfg=none ctermbg=12
hi CursorLine   cterm=none ctermfg=none ctermbg=8
hi CursorLineNr cterm=none ctermfg=4    ctermbg=8

" Output text
" Messages
hi ErrorMsg     cterm=none ctermfg=9    ctermbg=none
hi ModeMsg      cterm=bold ctermfg=6    ctermbg=none
hi MoreMsg      cterm=none ctermfg=4    ctermbg=none
hi WarningMsg   cterm=none ctermfg=11   ctermbg=none

" Misc.
hi Title        cterm=none ctermfg=4    ctermbg=none
hi Question     cterm=none ctermfg=4    ctermbg=none
hi SpecialKey   cterm=none ctermfg=12   ctermbg=none
hi NonText      cterm=none ctermfg=15   ctermbg=none
hi EndOfBuffer  cterm=none ctermfg=12   ctermbg=none

" Popup menu
hi Pmenu        cterm=bold ctermfg=4    ctermbg=0
hi PmenuSel     cterm=bold ctermfg=15   ctermbg=4
hi PmenuSbar    cterm=none ctermfg=none ctermbg=12
hi PmenuThumb   cterm=none ctermfg=none ctermbg=12

" Search
hi MatchParen   cterm=underline ctermfg=none ctermbg=none
hi IncSearch    cterm=none ctermfg=0    ctermbg=12
hi Search       cterm=none ctermfg=8    ctermbg=15
hi WildMenu     cterm=bold ctermfg=15   ctermbg=4

" Folding
hi Folded       cterm=none ctermfg=4    ctermbg=8
hi FoldColumn   cterm=none ctermfg=15   ctermbg=4

" Diff
hi DiffAdd      cterm=none ctermfg=10   ctermbg=none
hi DiffChange   cterm=none ctermfg=3    ctermbg=none
hi DiffDelete   cterm=none ctermfg=9    ctermbg=none
hi DiffText     cterm=none ctermfg=12   ctermbg=none

" Spellchecker
hi SpellBad     cterm=none ctermfg=9    ctermbg=none
hi SpellCap     cterm=none ctermfg=3    ctermbg=none
hi SpellLocal   cterm=none ctermfg=10   ctermbg=none
hi SpellRare    cterm=none ctermfg=11   ctermbg=none

" Miscellaneous
hi Directory    cterm=none ctermfg=4    ctermbg=none
hi LineNr       cterm=none ctermfg=8    ctermbg=none
hi VertSplit    cterm=none ctermfg=4    ctermbg=8
hi ColorColumn  cterm=none ctermfg=none ctermbg=8
hi SignColumn   cterm=none ctermfg=12   ctermbg=8
hi Conceal      cterm=none ctermfg=12   ctermbg=none

" Status line / Tab line
hi StatusLine   cterm=none ctermfg=0    ctermbg=6
hi StatusLineNC cterm=none ctermfg=none ctermbg=none
hi TabLine      cterm=none ctermfg=none ctermbg=none
hi TabLineFill  cterm=none ctermfg=none ctermbg=none
hi TabLineSel   cterm=none ctermfg=none ctermbg=none

"  Index | Name
" -------|-------------
"  0     | black
"  1     | darkred
"  2     | darkgreen
"  3     | darkyellow
"  4     | darkblue
"  5     | darkmagenta
"  6     | darkcyan
"  7     | gray
"  8     | darkgray
"  9     | red
"  10    | green
"  11    | yellow
"  12    | blue
"  13    | magenta
"  14    | cyan
"  15    | white
