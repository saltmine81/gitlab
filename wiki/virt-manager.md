# Virt-manager

## Setup virt-manager

[Source](https://computingforgeeks.com/complete-installation-of-kvmqemu-and-virt-manager-on-arch-linux-and-manjaro/)

    - `doas pacman -S qemu virt-manager virt-viewer dnsmasq vde2 bridge-utils openbsd-netcat`
    - `doas pacman -S ebtables iptables`
    - `doas systemctl enable --now libvirtd.service`
    - `doas vim /etc/libvirt/libvirtd.conf`
        - `unix_sock_group = "libvirt"`
        - `unix_sock_rw_perms = "0770"`
    - `doas usermod -a -G libvirt $(whoami)`
    - `doas systemctl restart libvirtd.service`

## If no network

`doas virsh net-list --all` and `doas virsh net-start default`

