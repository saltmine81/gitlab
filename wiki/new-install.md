# New install


## Install Arch base
[Arch Linux installation guide (https://wiki.archlinux.org/index.php/Installation_guide)](https://wiki.archlinux.org/index.php/Installation_guide)


### Boot loader

Install and enable update service:
    bootctl install
    systemctl enable systemd-boot-update.service

Configure:

    /boot/loader/loader.conf
    -----
    default arch.conf
    timeout 4
    console-mode max

    /boot/loader/entries/arch.conf
    -----
    title Arch
    linux /vmlinuz-linux-zen
    initrd /amd-ucode.img
    initrd /initramfs-linux-zen.img
    options root=/dev/nvme0n1p2 rw nowatchdog mitigations=off

### Network setup



## Tweaking and settings

- Set `noatime` [https://wiki.archlinux.org/index.php/Fstab#atime_options](https://wiki.archlinux.org/index.php/Fstab#atime_options)

- Disable hardware beep. Add `blacklist pcspkr` to `/etc/modprobe.d/nobeep.conf`

- Change default web browser with `xdg-settings set default-web-browser org.qutebrowser.qutebrowser.desktop`

- Install `chromium-widevine` (AUR) to be able to see DRM in Qutebrowser

- Install `brightnessctl` to adjust screen brightness. Example keyboard shortcuts: `brightnessctl s +10%` `brightnessctl s 10%-`

## Services

- Battery life: install `tlp` and `systemctl enable tlp.service`
- Periodic trim (SSD): install `util-linux` and `systemctl enable fstrim.timer`
- Refresh mirrorlist: install `reflector` and `systemctl enable reflector.timer`
- Firewall: install `ufw` and `systemctl enable ufw.service`
