# Generating a groff document

`groff-document.ms`

```
.TL
Generating groff documents
.AU
Magnus Ivarsson

.NH
Quick guide

.PP
Create \f[CW]document.ms\f[] and save.

.CW
groff -ms -Tps document.ms > temp.ps

ps2pdf temp.ps document.pdf

.NH
Inserting pictures

.PSPIC "pic.eps"
```
