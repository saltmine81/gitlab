# Git

## Setup git
Add global variables:

    git config --global user.name <USERNAME>
    git config --global user.email <EMAIL>


Setup SSH authentication ([https://docs.gitlab.com/ee/ssh/](https://docs.gitlab.com/ee/ssh/))

Pull from Gitlab:

    mkdir Directory
    git init
    git remote add origin project_link
    git pull origin master

project_link = for example `git@gitlab.com:saltmine81/gitlab.git (SSH)`
