# My current setup

Arch base, sway window manager. Gruvbox dark color scheme (mainly black, aqua and blue).

Some of the software I use:

- Essentials: `opendoas`, `fzf`, `git`, `htop`, `pacman-contrib`, `dunst`, `reflector`, `ufw`, and `tlp``
- Browser: `qutebrowser`
- Terminal: `foot`
- Shell: `bash`
- Text editor: `neovim`
- File manager: `fff`
- PDF viewer: `zathura`
- Image viewer: `imv`
- Video: `mpv`
- Font: `ttf-hack-nerd`
- Modeling and image editing; `blender` and `gimp`
- Games: `retroarch`

![My current setup](.screenshots/my-setup.jpg)

## configs

My different dotfiles.

## scripts

My own attempts at bash scripting for status menu info and other things.

## wallpapers

My own designs. Created with Blender.

## wiki

Collections of settings, snippets and stuff to make it all work. Learning something new every day.
